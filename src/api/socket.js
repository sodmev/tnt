import { io } from 'socket.io-client';
import { v4 as uuidv4 } from 'uuid';
import { debounce } from 'lodash';

const MAX_SUBSCRIBE_ATTEMPT = 5;

export class BaseSocket {
    static socket;

    socketBaseHost = '';
    socketBaseUrl = '/';
    listenersMap = new Map();

    constructor() {
        BaseSocket.socket = io(`${this.socketBaseHost}${this.socketBaseUrl}`, {
            reconnectionDelay: 3000,
            rejectUnauthorized: false,
            secure: false,
        });

        this.subscribeToBaseEvent();
    }

    getSocket = () => BaseSocket.socket;

    subscribeToBaseEvent = (socket)  => {
        socket.io.on('error', this.onError);
        socket.io.on('reconnect', this.onReconnect);
        socket.io.on('reconnect_failed', this.onReconnectError);
        socket.on('connect', this.onConnect(socket));
        socket.on('disconnect', this.onDisconnect);
    };
    onError = (error) => {
        console.error('websocket Error', error);
    };

    onReconnect = (attempt) => {
        console.log('websocket reconnection attempt', attempt);
    };

    onReconnectError = () => {
        console.error('websocket reconnect failing');
    };

    onConnect = (socket) => () => {
        console.log(`websocket ${socket.id}: ${socket.connected}`);
        this.resubscribeListeners();
    };

     onDisconnect = (reason) => {
        console.error('ws disconnected by reason: ', reason);
    };

     resubscribeListeners = () => {
        if (this.listenersMap.size) {
            this.listenersMap.forEach((eventArgs, id) => {
                const { callback, eventName } = eventArgs;
                BaseSocket.socket.on(eventName, (res) => {
                    if (res) {
                        const reconnectRes = { ...JSON.parse(res), id, reconnect: true };
                        callback(null, reconnectRes);
                    }
                });
            });
        }
    };

    connectToSocketRoom = async (payload) => {
        await new Promise((resolve) => {
            BaseSocket.socket.emit('Subscribe', payload, () => {
                resolve();
            });
        });
    };

    subscribeToEvent = (args, socket) => {
        const { eventName, callback, emitAttempt } = args;
        if (emitAttempt === MAX_SUBSCRIBE_ATTEMPT) {
            console.error('Не удалось отправить запрос');
            return;
        }

        /** Ожидаем подключения сокета */
        if (!this.getSocket().connected) {
            const attempt = emitAttempt ? emitAttempt + 1 : 1;
            debounce(this.subscribeToEvent, 500)({ eventName, callback, emitAttempt: attempt });
            return;
        }

        const subscribeId = uuidv4();
        this.listenersMap.set(subscribeId, { callback, eventName });

        socket.on(eventName, (res) => {
            if (res) {
                const parsedResponse = typeof res === 'string' ? JSON.parse(res) : res;
                callback(null, { ...parsedResponse, subscribeId });
            }
        });
    };

    unsubscribe = (args, socket) => {
        const { callback, id } = args;

        if (!id) {
            return;
        }

        this.listenersMap.delete(id);
        socket.removeListener(id, callback);
    };
}
