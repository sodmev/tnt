class Api {
    domain
    constructor(domain) {
        this.domain = domain
    }

    async getUsers(abortController) {
        try {
            const response = await fetch(`${this.domain}/users`, {
                signal: abortController.signal,
            });
            return await response.json();
        } catch (e) {
            if (e.name !== 'AbortError') {
                throw new Error('error')
            }
        }
    }
}

export const ApiData = new Api('https://jsonplaceholder.typicode.com')
