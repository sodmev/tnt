import {Box, Flex, Text} from "@chakra-ui/react";
import { Input } from '@chakra-ui/react'

export const ABB = (props) => {
    const data = props.data;

    return (
        <div className='abb-root'>
            <Text m={0} fontSize={'30px'}>ABB-ACS580</Text>
            <div className='abb-content'>
                <div className='abb-set'>
                    <Flex flexDir={'column'}>
                        <Box>
                            test
                        </Box>
                    </Flex>
                </div>
                <div className='abb-status'>
                    <Flex alignItems={'center'} gap={'16px'} mb={'20px'}>
                        <Box borderRadius={'50%'} w={'30px'} h={'30px'} background={'yellow'}
                             border={'1px solid transparent'}></Box>
                        Статус
                    </Flex>
                    <Box display={'flex'} mb={'12px'} justifyContent={'space-between'} alignItems={'center'} w={'100%'}>
                        <Text mr={'20px'}>Напряжение</Text>
                        <Input variant='outline' h={'34px'} value={data[0]?.id ?? ''} placeholder='0.0' readOnly/>
                    </Box>
                    <Box display={'flex'} mb={'12px'} justifyContent={'space-between'} alignItems={'center'} w={'100%'}>
                        <Text mr={'20px'}>Мощность</Text>
                        <Input variant='outline' h={'34px'} value={data[1]?.id ?? ''} alignItems={'center'}
                               placeholder='0.0' readOnly/>
                    </Box>
                    <Box display={'flex'} justifyContent={'space-between'} w={'100%'}>
                        <Text mr={'20px'}>Обороты</Text>
                        <Input variant='outline' h={'34px'} value={data[2]?.id ?? ''} placeholder='0.0' readOnly/>
                    </Box>
                </div>
            </div>
        </div>

    )
}
