import {Box, Input, Text} from "@chakra-ui/react";

export const Temp = (props) => {
    const data = props.data;
    return (
        <div className='temp-root'>
            <Box p={'20px 30px'} border={'1px solid'} background={'white'}>
                <Text>Датчик температуры</Text>
                <Box display={'flex'} mb={'12px'} justifyContent={'space-between'} alignItems={'center'} w={'70%'}>
                    <Text mr={'20px'}>Напряжение</Text>
                    <Input variant='outline' h={'34px'} value={data[0]?.id ?? ''} placeholder='0.0' readOnly/>
                </Box>
                <Box display={'flex'} mb={'12px'} justifyContent={'space-between'} alignItems={'center'} w={'70%'}>
                    <Text mr={'20px'}>Мощность</Text>
                    <Input variant='outline' h={'34px'} value={data[1]?.id ?? ''} alignItems={'center'} placeholder='0.0' readOnly/>
                </Box>
                <Box display={'flex'} justifyContent={'space-between'} w={'70%'}>
                    <Text mr={'20px'}>Обороты</Text>
                    <Input variant='outline' h={'34px'} value={data[2]?.id ?? ''} placeholder='0.0' readOnly/>
                </Box>
            </Box>
            <div>
                chart
            </div>
        </div>
    )
}
