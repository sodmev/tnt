import {useEffect, useState} from "react";
import {ApiData} from "./api/Api";
import {ABB} from "./components/ABB";
import {Temp} from "./components/Temp";
import {Vibro} from "./components/Vibro";
import {BaseSocket} from "./api/socket";

let ws = undefined;
const createSocket = () => {
    return new Promise((res) => {
        ws = new BaseSocket();
        ws.getSocket().on('connect', () => {
            ws.connectToSocketRoom();
            res();
        });
    });
}

export const App = () => {
    const [users, setUsers] = useState([])

    useEffect(() => {
        const abortController = new AbortController();

        ApiData.getUsers(abortController).then((data) => {
          setUsers(data)
        })

        return () => {
          abortController.abort('unmount component')
        }
    }, [])

    useEffect(() => {
        createSocket().then(() => {
            ws.subscribeToEvent({
                eventName: 'eventName',
                callback: (err, res) => {
                    console.log(err, res)
                },
            });
        })
    }, []);



  return (
      <div className="root">
        <div className='content'>
            <ABB data={users} />
            <div className={'params'}>
                <Temp data={users} />
                <Vibro data={users} />
            </div>
        </div>
      </div>
  )
}
